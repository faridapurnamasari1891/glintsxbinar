//read/write file
const fs = require('fs')

/* Start make promise */
const readFile = a=> b => new Promise((c, d) => {
 fs.readFile(b, a, (err, content) => {
   if (err) return c(err)
   return d(content)
 })
})

const writeFile = (b, content) => new Promise((c, d) => {
 fs.writeFile(b, content, err => {
   if (err) return d(err)
   return c()
 })
})
/* End make promise */


/* Make options variable for fs */
const
 read = readFile('utf-8')

/* End make options variable for fs */

/* async function */
async function mergedContent() {
 try {
   /* This is recommended */
   const result = await Promise.all([
     read('contents/content1.txt'),
     read('contents/content2.txt'),
     read('contents/content3.txt'),
     read('contents/content4.txt'),
     read('contents/content5.txt'),
     read('contents/content6.txt'),
     read('contents/content7.txt'),
     read('contents/content8.txt'),
     read('contents/content9.txt'),
     read('contents/content10.txt')
   ])
   /* end recommended */
   await writeFile('contents/result.txt', result.join(''))
 } catch (e) {
   throw e
 }

 // The best practice is:
 // return promise, not return value of promise
 // not also return use await
 return read('contents/result.txt')
}

// console.log(mergedContent()) // Promise<Pending>

/* Start promise */
mergedContent()
 .then(result => {
   console.log(result) // content of result.txt
 }).catch(err => {
   console.log('Error to read/write file, error: ', err)
 }).finally(() => {
   console.log('Mantappp!!');
 })
/* end promise */
