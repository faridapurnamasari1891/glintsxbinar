function TotalBayarNormal (diskon,hargaasli){
  return hargaasli-(diskon/100*hargaasli)
}

let harga1=TotalBayarNormal(10,100000)
let harga2=TotalBayarNormal(10,200000)


console.log(harga1);
console.log(harga2)


let TotalBayarArrow=(diskon,hargaasli)=>{
  return hargaasli-(diskon/100*hargaasli)
}

let diskonarrow=10

let harga4=TotalBayarArrow(diskonarrow,400000)
let harga5=TotalBayarArrow(diskonarrow,500000)

console.log(harga4);
console.log(harga5)


let TotalBayarCurrying=diskon=>hargaasli=>{
  return hargaasli-(diskon/100*hargaasli)
}

console.log(TotalBayarCurrying(10)(250000));
let diskoncurrying=TotalBayarCurrying(10)

let harga7=diskoncurrying(700000)
let harga8=diskoncurrying(800000)

console.log(harga7);
console.log(harga8);
