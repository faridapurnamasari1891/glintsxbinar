const bangun=require('./bangun.js')
const bangunruang=require('./bangunruang.js')



class Balok extends bangunruang{
  constructor(panjang, lebar, tinggi){
    super('Balok')
    this.panjang=panjang
    this.lebar=lebar
    this.tinggi=tinggi
  }


menghitungVolumeBalok() {
  return this.panjang*this.lebar*this.tinggi
}

menghitungLuasBalok() {
  return 2*(this.panjang+this.lebar)
}
}

module.exports=Balok
