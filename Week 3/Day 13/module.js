const balok = require ('./balok.js')


class Module {
  constructor(){
    this.name = "Menghitung Bangun Ruang"
  }


  menghitungVolumeBalok1(panjang, lebar, tinggi){
    let hitungVolumeBalok= new balok(panjang, lebar, tinggi)
    return hitungVolumeBalok.menghitungVolumeBalok()
  }

  menghitungLuasBalok1(panjang,lebar,tinggi){
    let hitungLuasBalok=new balok(panjang, lebar, tinggi)
    return hitungLuasBalok.menghitungLuasBalok()
  }


}

module.exports = Module
