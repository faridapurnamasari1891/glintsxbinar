const bangun=require ('./bangun.js')


class BangunRuang extends bangun {

  // Make constructor with name variable/property
  constructor(name) {
    super(name)

    if (this.constructor === BangunRuang) {
      throw new Error('This is abstract!')
    }
  }

  // menghitungLuas instance method
  menghitungVolume() {
    console.log('Volume bangun ruang');
  }

  menghitungLuas(){
    console.log('Luas bangun ruang')
  }
}

module.exports = BangunRuang
