const express=require('express')
const app = express()

app.use(express.static('public'))
app.get('/', (req,res)=>{
  console.log('You are accessing Hello World');
  res.render('top.ejs')
})

app.get('/hello', (req,res)=>{
  console.log('You are accessing Hello.ejs');
  res.render('hello.ejs')
})

app.get('/index', (req,res)=>{
  console.log('You are accessing index');
  res.render('index.ejs')
})


app.listen(3000)
