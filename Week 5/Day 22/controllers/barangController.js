const client = require('../models/connection.js') // import connection
const { ObjectId } = require('mongodb') // Import ObjectId from mongodb

class BarangController {

  // get All data
  async getAll(req, res) {
    const penjualan = client.db('penjualan') // Connect to penjualan database
    const barang = penjualan.collection('barang') // Connect to transaksi collection / table

    // find all transaksi data
    barang.find({}).toArray().then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  // get one data
  async getOne(req, res) {
    const penjualan = client.db('penjualan') // Connect to penjualan database
    const barang = penjualan.collection('barang') // Connect to transaksi collection / table

    // Find one data
    barang.findOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  // create a data
  async create(req, res) {
    const penjualan = client.db('penjualan') // Connect to penjualan database
    const barang = penjualan.collection('barang') // Connect to transaksi collection / table

    // Get data barang depends on req.body.id_barang


    // Insert data transaksi
    barang.insertOne({
      nama:req.body.nama,
      harga:req.body.harga,
      id_pemasok:req.body.id_pemasok
    }).then(result => {
      res.json({
        status: "success",
        data: result.ops[0]
      })
    })
  }

  // update a data
  async update(req, res) {
    const penjualan = client.db('penjualan') // Connect to penjualan database
    const barang = penjualan.collection('barang') // Connect to transaksi collection / table


    // Update data depends on transaksi
    barang.updateOne({
      _id: new ObjectId(req.params.id)
    }, {
      $set: {
        nama:req.body.nama,
        harga:req.body.harga,
        id_pemasok:req.body.id_pemasok
      }
    }).then(() => {
      return barang.findOne({
        _id: new ObjectId(req.params.id)
      })
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // delete data
  async delete(req, res) {
    const penjualan = client.db('penjualan') // Connect to penjualan database
    const barang = penjualan.collection('barang') // Connect to transaksi collection / table

    // delete data depends on req.params.id
  barang.deleteOne({
      _id: new ObjectId(req.params.id)
    }).then(result => {
      res.json({
        status: 'success',
        data: null
      })
    })
  }

}

module.exports = new BarangController;
