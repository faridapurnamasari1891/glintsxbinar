const { check, validationResult, matchedData, sanitize } = require('express-validator'); // Import express-validator
const client = require('../../models/connection.js') // Import connection
const { ObjectId } = require('mongodb') // Import ObjectId

module.exports = {
  create: [
    check('nama').isAlpha(),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next()
    }
  ],
  update: [
    check('id').custom(value => {
    return client.db('penjualan').collection('pemasok').findOne({

        _id: new ObjectId(value)

    }). then(p => {
      if (!p) {
        throw new Error("ID pemasok tidak ada!")
      }
    })
  }),

  (req,res,next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({
        errors: errors.mapped()
      })
    }
    next();
  }
  ]
}
