const express = require('express') // Import express
const router = express.Router() // Make express router
const PemasokController = require('../controllers/pemasokController.js') // Import pemasokController from controllers directory
const pemasokValidator = require('../middlewares/validators/pemasokValidator.js') // Import pemasokValidator

router.get('/', PemasokController.getAll) // If accessing localhost:3000/pemasok/, it will go to getAll method in PemasokController
router.get('/:id', PemasokController.getOne) // If accessing localhost:3000/pemasok/:id, it will go to getOne
router.post('/create', pemasokValidator.create, PemasokController.create) // If accessing localhost:3000/create, it will go to pemasokValidator and create function in controller
router.put('/update/:id', pemasokValidator.update, PemasokController.update) // If accessing localhost:3000/update/:id, it will go to pemasokValidator and update function in controller
router.delete('/delete/:id', PemasokController.delete) // If accessing localhost:3000/delete/:id, it will go to delete function in pemasokController

module.exports = router; // Export router
