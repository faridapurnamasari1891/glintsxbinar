const {
  pelanggan
  } = require('../models') // Import pelanggan, pelanggan, transaksi models

// make TransaksiController class
class PelangganController {

  // Get all data function
  async getAll(req, res) {
    // Find all transaksi collection data
    pelanggan.find({}).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Get one data
  async getOne(req, res) {
    // Find one
    pelanggan.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Create data
  async create(req, res) {

  // Create transaksi data
    pelanggan.create({
      nama:req.body.nama,
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  // Update data
  async update(req, res) {



    // Update data depends on _id
    pelanggan.findOneAndUpdate({
      _id: req.params.id
    }, {
      "nama":req.body.nama,
        }).then(() => {
      return pelanggan.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "success",
        result: result
      })
    })
  }

  // Delete data
  async delete(req, res) {
    // Delete data
    pelanggan.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: 'success',
        data: null
      })
    })
  }

}

module.exports = new PelangganController; // Export TransaksiController class
