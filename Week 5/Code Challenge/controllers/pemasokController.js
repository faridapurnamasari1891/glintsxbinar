const {
  pemasok
  } = require('../models') // Import pemasok, pemasok, transaksi models

// make TransaksiController class
class PemasokController {

  // Get all data function
  async getAll(req, res) {
    // Find all transaksi collection data
    pemasok.find({}).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Get one data
  async getOne(req, res) {
    // Find one
    pemasok.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Create data
  async create(req, res) {

  // Create transaksi data
    pemasok.create({
      nama:req.body.nama,
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  // Update data
  async update(req, res) {



    // Update data depends on _id
    pemasok.findOneAndUpdate({
      _id: req.params.id
    }, {
      "nama":req.body.nama,
        }).then(() => {
      return pemasok.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "success",
        result: result
      })
    })
  }

  // Delete data
  async delete(req, res) {
    // Delete data
    pemasok.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: 'success',
        data: null
      })
    })
  }

}

module.exports = new PemasokController; // Export TransaksiController class
