const {
  barang,
  pemasok
  } = require('../models') // Import barang, pelanggan, transaksi models

// make TransaksiController class
class BarangController {

  // Get all data function
  async getAll(req, res) {
    // Find all transaksi collection data
    barang.find({}).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Get one data
  async getOne(req, res) {
    // Find one
    barang.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  // Create data
  async create(req, res) {
    // find barang and pelanggan
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])

  // Create transaksi data
    barang.create({
      pemasok: data[0],
      nama:req.body.nama,
      harga: eval(req.body.harga)
    }).then(result => {
      res.json({
        status: "success",
        data: result
      })
    })
  }

  // Update data
  async update(req, res) {
    // find barang and pelanggan
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])


    // Update data depends on _id
    barang.findOneAndUpdate({
      _id: req.params.id
    }, {
      "pemasok": data[0],
      "nama":req.body.nama,
      "harga": eval(req.body.harga),
    }).then(() => {
      return barang.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "success",
        result: result
      })
    })
  }

  // Delete data
  async delete(req, res) {
    // Delete data
    barang.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: 'success',
        data: null
      })
    })
  }

}

module.exports = new BarangController; // Export TransaksiController class
