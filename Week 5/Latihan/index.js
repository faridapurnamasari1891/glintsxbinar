const express=require('express');
const app=express()
const bodyParser=require('body-parser')
const mahasiswaRoutes=require('./routes/mahasiswaRoutes.js')
const nilaiRoutes=require('./routes/nilaiRoutes.js')
const matkulRoutes=require('./routes/matkulRoutes.js')
const fakultasRoutes=require('./routes/fakultasRoutes.js')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended=True}));

app.use('/mahasiswa',mahasiswaRoutes);
app.use('/nilai', nilaiRoutes);
app.use('/matkul',matkulRoutes);
app.use('/fakultas',fakultasRoutes)

app.listen(3000)
