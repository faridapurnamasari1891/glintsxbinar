const { Transaksi, Barang, Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params

class BarangController {

  constructor() {
    Pemasok.hasMany(Barang, {
      foreignKey: 'id_pemasok'
    })
    Barang.hasMany(Transaksi, {
      foreignKey: 'id_barang'
    })
    Transaksi.belongsTo(Barang, {
      foreignKey: 'id_barang'
    })
    Pemasok.belongsTo(Barang, {
      foreignKey: 'id_pemasok'
    })
  }
  // Get All data from transaksi
    async getAll(req, res) {
      Barang.findAll({ // find all data of Transaksi table
        attributes: ['id','nama', 'harga', ['createdAt', 'waktu']], // just these attributes that showed
        include: {
          model: Pemasok,
          attributes: ['nama'] // just this attrubute from Pemasok that showed
        }
      }).then(barang => {
        res.json(barang) // Send response JSON and get all of Transaksi table
      })
    }
    // Get One data from transaksi
  async getOne(req, res) {
    Barang.findOne({ // find one data of Transaksi table
      where: {
        id: req.params.id // where id of Transaksi table is equal to req.params.id
      },
      attributes: ['id','nama', 'harga', ['createdAt', 'waktu']], // just these attributes that showed
      include: {
        model: Pemasok,
        attributes: ['nama'] // just this attrubute from Pemasok that showed
      }
    }).then(barang => {
      res.json(barang) // Send response JSON and get one of Transaksi table depend on req.params.id
    })
  }
  // Create Transaksi data
  async create(req, res) {
    Barang.create({
        id_barang: req.body.id_barang,
        nama: req.body.nama,
        id_pemasok: req.body.id_pemasok,
        jumlah: req.body.jumlah,
        total: total,
      })
    .then(newbarang => {
      // Send response JSON and get one of Transaksi table that we've created
      res.json({
        "status": "success",
        "message": "barang added",
        "data": newbarang
      })
    })
  }
  // Update Transaksi data
  async update(req, res) {

      // Make update query
      Barang.update({


        id_pemasok: req.body.id_pemasok,
        nama    : req.body.nama,
        harga : req.body.harga,

      })

      // Transaksi table update data
      Barang.update(update, {
        where: {
          id: req.params.id
        }

      }).then(affectedRow => {
      return Barang.findOne({ // find one data of Transaksi table
        where: {
          id: req.params.id  // where id of Transaksi table is equal to req.params.id
        }
      })
    }).then(b => {
      // Send response JSON and get one of Transaksi table that we've updated
      res.json({
        "status": "success",
        "message": "barang updated",
        "data": b
      })
    })
  }
  // Soft delete Transaksi data
    async delete(req, res) {
      Barang.destroy({  // Delete data from Transaksi table
        where: {
          id: req.params.id  // Where id of Transaksi table is equal to req.params.id
        }
      }).then(affectedRow => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            "status": "success",
            "message": "barang deleted",
            "data": null
          }
        }

        // If failed, it will return this JSON
        return {
          "status": "error",
          "message": "Failed",
          "data": null
        }
      }).then(r => {
        res.json(r) // Send response JSON depends on failed or success
      })
    }

}

module.exports = new BarangController;
