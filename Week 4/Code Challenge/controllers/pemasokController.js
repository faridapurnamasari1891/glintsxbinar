const { Barang, Pemasok } = require("../models")
const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params

class PemasokController {

  constructor() {

    Pemasok.hasMany(Barang, {
      foreignKey: 'id_pemasok'
    })
    Barang.belongsTo(Pemasok, {
      foreignKey: 'id_pemasok'
    })

  }
  // Get All data from barang
    async getAll(req, res) {
      Pemasok.findAll({ // find all data of Barang table
        attributes: ['id','nama', ['createdAt', 'waktu']], // just these attributes that showed

      }).then(pemasok => {
        res.json(pemasok) // Send response JSON and get all of Barang table
      })
    }
    // Get One data from barang
  async getOne(req, res) {
    Pemasok.findOne({ // find one data of Barang table
      where: {
        id: req.params.id // where id of Barang table is equal to req.params.id
      },
      attributes: ['id','nama', ['createdAt', 'waktu']], // just these attributes that showed

    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get one of Barang table depend on req.params.id
    })
  }
  // Create Barang data
  async create(req, res) {
    Pemasok.create({
        id_pemasok: req.body.id_pemasok,
        nama: req.body.nama,
      })
    .then(newpemasok => {
      // Send response JSON and get one of Barang table that we've created
      res.json({
        "status": "success",
        "message": "pemasok added",
        "data": newpemasok
      })
    })
  }
  // Update Barang data
  async update(req, res) {
      // Make update query
      Pemasok.update({
        id_pemasok: req.body.id_pemasok,
        nama: req.body.nama,

            })
    return Pemasok.update(update, {
        where: {
          id: req.params.id
        }
      }).then(affectedRow => {
      return Pemasok.findOne({ // find one data of Barang table
        where: {
          id: req.params.id  // where id of Barang table is equal to req.params.id
        }
      })
    }).then(b => {
      // Send response JSON and get one of Barang table that we've updated
      res.json({
        "status": "success",
        "message": "pemasok updated",
        "data": b
      })
    })
  }
  // Soft delete Barang data
    async delete(req, res) {
      Pemasok.destroy({  // Delete data from Barang table
        where: {
          id: req.params.id  // Where id of Barang table is equal to req.params.id
        }
      }).then(affectedRow => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            "status": "success",
            "message": "pemasok deleted",
            "data": null
          }
        }

        // If failed, it will return this JSON
        return {
          "status": "error",
          "message": "Failed",
          "data": null
        }
      }).then(r => {
        res.json(r) // Send response JSON depends on failed or success
      })
    }

}

module.exports = new PemasokController;
