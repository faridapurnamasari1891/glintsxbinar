const express=require ('express')
const app = express()
const transaksiRoutes=require('./router/transaksiRouter.js')

app.use(express.urlencoded({extended: false}));
app.use('/transaksi', transaksiRoutes)



app.listen(3000)
