const connection = require ('../models/connection.js')


class TransaksiController{

  async getAll(req,res){
    try{
    var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id"
      connection.query(sql, function (err,result){
        if (err) throw err;
        res.json({
          status:"success",
          data:result
        })
      });
    }
    catch(e){
      res.json({
        status:"Error"
      })
    }
  }



  async getOne(req,res){
    try{
       var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?"
      connection.query(sql,[req.params.id], function (err,result){
        if (err) throw err;
        res.json({
          status:"success",
          data:result[0]
        })
      });
    }
    catch(e){
      res.json({
        status:"Error"
      })
      }
    }

    async create(req, res) {
    try {
      connection.query(
        'INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)',
        [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, req.body.total],
        (e, result) => {
          if (e) throw e; // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async update(req, res) {
  try {

    var sql = 'UPDATE transaksi t SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ? WHERE id = ?'

    connection.query(
      sql,
      [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, req.body.total, req.params.id],
      (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          });
        } // If error

        // If success it will return JSON of result
        res.json({
          status: 'Success',
          data: result
        })
      }
    )
  } catch (err) {
    // If error will be send Error JSON
    res.json({
      status: "Error",
      error: err
    })
  }
}
async delete(req, res) {
    try {

      var sql = 'DELETE FROM transaksi t WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

  }



module.exports=new TransaksiController
