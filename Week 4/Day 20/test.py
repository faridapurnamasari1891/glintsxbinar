arr=[101020
     012302
     013201
     201103
     103201
     103012]

# Complete the hourglassSum function below.
def hourglassSum(arr):
    maxhourglass=-63
    for i in range (2,4):
        for j in range (2,4):
            sum=0
            sum+=arr[i-1][j-1]
            sum+=arr[i-1][j]
            sum+=arr[i-1][j+1]
            sum+=arr[i][j]
            sum+=arr[i+1][j-1]
            sum+=arr[i+1][j]
            sum+=arr[i+1][j+1]
            currentHG=sum
            if currentHG>maxhourglass:
                maxhourglass=currentHG
    print(maxhourglass)
