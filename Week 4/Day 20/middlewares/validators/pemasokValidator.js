// Import models
const {Pemasok} = require('../../models')

// Import validator modules
const {check,validationResult,matchedData,sanitize} = require('express-validator')

module.exports = {
  create:[
    check('nama').isAlpha().custom(value => {
      return Pemasok.findOne({
        where: {
          nama:value
        }
      }). then (p => {
        if (p) {
          throw new Error("Nama pemasok sudah ada!")
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id').isNumeric().custom(value => {
      return Pemasok.findOne({
        where: {
          id:value
        }
      }). then(p => {
        if (!p) {
          throw new Error("ID pemasok tidak ada!")
        }
      })
    }),
    check('nama').isAlpha().custom(value =>{
      return Pemasok.findOne({
        where: {
          nama:value
        }
      }).then(p => {
        if (p) {
          throw new Error("Nama pemasok sudah ada!")
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').isNumeric().custom(value =>{
      return Pemasok.findOne({
        where: {
          id:value
        }
      }).then(t =>{
        if (!t) {
          throw new Error('ID pemasok tidak ada!');
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
