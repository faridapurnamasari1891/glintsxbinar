// Import models
const {Pelanggan} = require('../../models')

// Import validator modules
const {check,validationResult,matchedData,sanitize} = require('express-validator')

module.exports = {
  create:[
    check('nama').isAlpha().custom(value => {
      return Pelanggan.findOne({
        where: {
          nama:value
        }
      }). then (p => {
        if (p) {
          throw new Error("Nama pelanggan sudah ada!")
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ],

  update: [
    check('id').isNumeric().custom(value => {
      return Pelanggan.findOne({
        where: {
          id:value
        }
      }). then(p => {
        if (!p) {
          throw new Error("ID pelanggan tidak ada!")
        }
      })
    }),
    check('nama').isAlpha().custom(value =>{
      return Pelanggan.findOne({
        where: {
          nama:value
        }
      }).then(p => {
        if (p) {
          throw new Error("Nama pelanggan sudah ada!")
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete: [
    check('id').isNumeric().custom(value =>{
      return Pelanggan.findOne({
        where: {
          id:value
        }
      }).then(t =>{
        if (!t) {
          throw new Error('ID pelanggan tidak ada!');
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    }
  ]
}
