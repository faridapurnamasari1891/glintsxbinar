'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pelanggan extends Model {
        static associate(models) {}
  };
  Pelanggan.init({
    nama: DataTypes.STRING
  },
    {
      sequelize,
      paranoid: true,
      timestamps: true,
      freezeTableName: true,
      modelName:'Pelanggan'


  });
  return Pelanggan;
};
