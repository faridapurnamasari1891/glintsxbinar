const {Pemasok} = require("../models")

class PemasokController {
  async getAll(req,res) {
    Pemasok.findAll({
      attributes: ["id","nama", ['createdAt','waktu']]
    }).then(pemasok => {
      res.json(pemasok)
    })
  }

  async getOne(req,res) {
    Pemasok.findOne({
      where: {
        id:req.params.id
      },
      attributes: ['id','nama', ['createdAt', 'waktu']]
    }).then(pemasok => {
      if (!pemasok) {
        return res.status(422).json({
          "status":"failed",
          "message":"ID pemasok tidak ada!",
          "data":null
        })
      }
      res.json(pemasok)
    })
  }

  async create(req,res) {
    Pemasok.create({
      nama:req.body.nama
    }).then(result => {
      res.json({
        "status":"success",
        "message":"pemasok added",
        "data": result
      })
    })
  }

  async update(req,res) {
    let update = {
      nama:req.body.nama
    }
    Pemasok.update(update, {
      where: {
        id:req.params.id
      }
    }).then(result => {
      return Pemasok.findOne({
        where: {
          id:req.params.id
        }
      })
    }).then(result => {
      res.json({
        "status":"success",
        "message":"pemasok updated",
        "data":result
      })
    })
  }

  async delete(req,res) {
    Pemasok.destroy({
      where:{
        id:req.params.id
      }
    }).then(result => {
      if(result) {
        return {
          "status":"success",
          "message":"pemasok deleted",
          "data":null
        }
      }
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r=>{
      res.json(r)
    })
  }
}

module.exports = new PemasokController;
